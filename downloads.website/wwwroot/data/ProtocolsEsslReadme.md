﻿# Protocols.ESSL
## ESSL - EventSource Specification Language

### Components
* **ESSL.Parser** - A library that uses <a href="https://www.antlr.org/" target="_blank">Antlr4</a> to parse ESSL grammar files.
* **ESSL.DOM** - A Document Object Model library that is populated by the ESSL Parser.  This model is heavily
  used to drive all related file generation.
* **ESSL.Generator.Lib** - An implementation library that executes file generation, combining <a href="https://github.com/antlr/antlrcs" target="_blank">StringTemplate4</a>
  templates in the ESSL.Templates folder with a populated ESSL.DOM to generate text output of services, API wrappers, PlantUml diagrams, Client Types, Web Pages, etc.
* **ESSL.Templates** - A collection of <a href="https://github.com/antlr/antlrcs" target="_blank">StringTemplate4</a> templates (.stg files) that define the structural content
  of the files being generated.
* **ESSL.Generator** - A command-line executable which combines the various components discussed here to
  provide simple compilation, and generation of any of the various types of output.

### Getting Started
To start, you will at minimum need ESSL source from the <a href="https://bitbucket.org/sweetbridge/eventstore/src/master/" target="_blank">EventStore</a>
solution.  This solution contains multiple projects, including EventStore and Protocols.Config.  These two projects are required to begin any new
projects.

Each CSharp project contains an ESSL folder typically directly inside it.
* In the EventStore it is at ESSLTypes/ESSL, which is done to enable the hybrid (partially generated)
  approach taken by this foundational project.
* All other projects by convention contain the ESSL source files in a top-level ESSL folder.
* This folder contains exactly one ESSL project file (.esslproj), which is the ESSL equivalent of a .csproj file.
* The folder also contains one or more .essl source files.  Together, the .esslproj and .essl files make up the
  source code of an ESSL project.
* When the service is generated, ESSL.Generator automatically adds folders, generated C# source code and to fill out a ready to compile C# project.
  * Note that the ESSL source lives with its corresponding CSharp project.  This conveniently allows the ESSL source to be in
    a well-known location, and be part of the same source-code repository.
#### ESSL generates source, not compiled files
Unlike a language like C#, Java, etc., which creates a compiled form such as a dll, or jar, ESSL does not (currently) persist to a compiled
form.  This means that when compiling ESSL, the ESSL projects which it references must be available to the
ESSL compiler.  Since solutions typically reference multiple projects which may well exist in multiple repositories, the distributed
nature of the ESSL source (since it lives with each generated project) may be a nuisance to work with.

Package Managers, such as NuGet for .net, and Maven for Java exist exactly for addressing this issue.  They manage an make available the
compiled form.  Since ESSL does not persist a compiled form, the source itself is necessary.
##### Two copies of ESSL source
To ease this, the ESSL.Generator gives you a choice of compiling ESSL from the distributed projects (--project) or
from a combined local folder (--local).  Of course this means your local ESSL source could get out of sync with the distributed
project source, so ESSL.Generator provides a "compare" command that reports any differences between the local and
project sources.
### Your first ESSL Project
It is easiest to have a single folder which you place all relevant repositories inside.  For this example, we will call it "Root".
Select where you wish this to be, choose a name for your "Root" folder and create it now.  To jumpstart things, we will clone the
essl.combinedsource repository and then create your local "HelloEsslRepo" repository folder.

From your "Root" folder, adapt one of the two lines below based on whether you
use SSH or HTTPS to access BitBucket.  Alternatively, use your favorite client like SourceTree, GitHub Desktop, etc. to clone the ESSL.CombinedSource repository.
Make sure your current directory is "Root" when cloning the public "ESSL.CombinedSource" repository.

```shell
# if using SSH:
git clone git@bitbucket.org:sweetbridge/essl.combinedsource.git

# if using HTTPS and your BitBucket account:
git clone https://Your-Account@bitbucket.org/sweetbridge/essl.combinedsource.git
```

When you have completed this, you should have an "essl.combinedsource" folder under your Root folder.  For purposes of getting started,
we will simply copy a few folders and files from it into your own folders.  We will not directly use the essl.combinedsource folder.

This script will copy two of the folders in "essl.combinedsource" into two corresponding folders under "HelloEsslRepo/EsslLocalSource".  Note
that this and other "terminal" scripts in this example were created on OSX.  If using Windows or Linux, you may need to
make alterations for the appropriate command-prompt environment.  To start, make sure your current directory is set to the "Root" you
created, then execute this script:

```shell
# Create a repository directory called HelloEsslRepo and within that a EsslLocalSource folder to hold the ESSL source we will work with
mkdir HelloEsslRepo
mkdir HelloEsslRepo/EsslLocalSource
cd HelloEsslRepo/EsslLocalSource
# Copy the EventStore ESSL project
cp -r ../../essl.combinedsource/EventStore .
# Copy the Protocols.Config ESSL project
cp -r ../../essl.combinedsource/Config .
# Copy the esslsln file
cp ../../essl.combinedsource/Protocols.esslsln HelloEssl.esslsln
# Create a HelloEssl folder where your first ESSL project will be located
mkdir HelloEssl
```

Before continuing, take a moment to examine everything under "Root".  You will see
* **essl.combinedsource** - a folder containing the contents of the cloned Essl.CombinedSource repository, including an ESSL project folder for each project.
* **HelloEsslRepo** - a folder which will hold the solution you are creating, including EsslLocalSource (see below),
  and later on, generated CSharp projects in your solution (repository)
  * **EsslLocalSource** - your local ESSL source, which is your equivalent of Essl.Combined source
    * **Config** - Contains ESSL source for the Config project (copied from essl.combinedsource)
    * **EventStore** - Contains ESSL source for the EventStore project (copied from essl.combinedsource)
    * **HelloEssl** - Your project folder, where you will be editing your ESSL source
    * **HelloEssl.esslsln** - Your ESSL solution file which will describe all the projects in your solution
  * *SB.HelloEssl* - Will be created later when you "generate" your service.  This will be the CSharp source of the SB.HelloEssl project.
* *EventStore* - The EventStore repository, which could be cloned.  If you choose to generate the EventStore or Protocols.Config service, they will appear in here
  * *EventStore* - Later, contains either the cloned or generated EventStore CSharp project.
  * *Protocols.Config* - Later, contains either the cloned or generated Protocols.Config CSharp project (which is in the EventStore repository).

From the above, it should become more clear that repositories exist under "Root".  Each repository, will have its own structure which is under
it.  For your purposes, HelloEsslRepo/EsslLocalSource/HelloEssl is where you will be editing your ESSL project.

This structure of all repositories being siblings of each other is good practice, even without ESSL.  It allows for relative "project references" that allow references in one
project to reference another project using relative paths even if it is part of a different repository.  This is very useful
when debugging, although for distribution, cross-repository references should be references to the compiled form using a PackageManager.

Now that the EsslLocalSource folder contains all necessary referenced files, we need to edit HelloEssl.esslsln.  It simply contains JSON configuration
information, so you can edit it with your editor of choice.  However, we suggest opening the HelloEsslRepo folder in VSCode, with the
[VSCode ESSL extension](https://downloads.sweetbridge.com/essl/vscode-extension) installed, then navigating to EsslLocalSource folder and opening
HelloEssl.esslsln.

Once open, replace the content of the JSON object, inside the { }, with the content below.  Start the replace after the opening
comment (yes comments are non-standard in JSON, but the VSCode edit and ESSL allow them).

```json
  "root" : "../../",  // root is required, and defaults to the path where this solution file was read from
  "eventStoreRepo" : "{root}/EventStore",
  "helloEsslRepo" : "{root}/HelloEsslRepo",
  "esslFolders": [
    {
      "path": "{eventStoreRepo}/Protocols.Config/",
      "action": "reference"
    },
    {
      "path": "{eventStoreRepo}/EventStore/ESSLTypes",
      "action": "reference"
    },
    {
      "path": "{helloEsslRepo}/SB.HelloEssl",
      "action": "generate"
    }
  ]
```
Note that rootPath is set to two levels higher than where the file lives.  This is because this file is located two levels below (HelloEsslRepo/EsslLocalSource) relative
to the root where all repositories each have their own folder.  Following the definition of "root", other "variables" may be declared, such that they can resolve from
"root", or each other.  Finally, "esslFolders" must be declared in the form shown.  This enables ESSL.Generator to derive all relevant file locations
starting the with esslsln file.

If using the VSCode extension, select any open .essl, .esslproj, or .esslsln file and right click on it.  Select
"Compile Services", then check the service(s) you wish to compile, and click "OK".  The service(s) you selected will
be compiled, along with any dependencies.  (HelloEssl will not show up since we have not created a project file for it yet).

If using command line, open Terminal, set the working directory to your EsslLocalSource directory (Root/HelloEsslRepo/EsslLocalSource) and execute the compiler.
You will get an error, which will be discussed shortly.
```shell
# compile based on the current local folder (HelloEssl)
ESSL.Generator compile ./ --local
# or, compile by pointing directly to the HelloEssl.esslsln file
ESSL.Generator compile HelloEssl.esslsln --local
```
Of course the ESSL.Generator must be findable from the PATH environment variable.  This is enough to tell ESSL that it should compile the
solution file and everything it references, using the local ESSL project folders.  If the project folders have yet to be created, it
will give errors, hopefully guiding you to what needs to be corrected.

You can expect an error complaining that it could not find a project for HelloEssl.  That makes sense since we haven't created it yet.  We'll get to it
shortly.

```text
Pro Tip:  You can add --verbose (or -v) option when using the command-line ESSL Generator to get additional diagnostic information.
          This option can be used with many Generator verbs.
```

The EsslLocalSource folder should contain the .esslsln file and three subfolders, one for each esslFolder entry in the solution file.
The folder names do not have to exactly match.  In this case the folders will be Config, EventStore, and HelloEssl.

Each of these folders must contain one .esslproj and the corresponding .essl source files for that project.  For both Config and the EventStore,
the files were previously copied in from essl.combinedsource.

Now you are ready to create your own HelloEssl .essl files and corresponding .esslproj file and place them in the EsslLocalSource/HelloEssl folder.  Go into that folder
and create a HelloEssl.esslproj file inside it.  Paste the content below inside it.

```json
{
  "serviceName": "HelloEssl",
  "namespace": "SB.HelloEssl",
  "servicePath": "{helloEsslRepo}/{namespace}",
  "apiPath": "{helloEsslRepo}/{namespace}.API",
  "usesServiceConfig": true,
  "dependencies": ["EventStore"]
}
```
Notice that servicePath and apiPath are both defined using a combination of derived values declared in the
.esslsln and this .esslproj files.  ServiceName is the short name for the service, and may or may
not be the same as Namespace.  By convention, notice that the ServicePath is the project root {helloEsslRepo}, and
then the {namespace}.  When the service itself is generated, it will have the name {namespace}.csproj.  So, this
project will be generated as {root}/HelloEsslRepo/SB.helloEssl/SB.HelloEssl.csproj.  Or, *solutionFolder/projectName*.
Of course, the project location for the ESSL files will be {root}/SB.helloEssl/ESSL.

If using the VSCode extension, select any open .essl, .esslproj, or .esslsln file and right click on it.  Select
"Compile Services", then check the service(s) you wish to compile, and click "OK".  The service(s) you selected will
be compiled, along with any dependencies.

If using command line, make sure your current directory is <root>/HelloEsslRepo/EsslLocalSource.  Verify that the ESSLSource folder and its contents
are set up properly by compiling:
```shell
ESSL.Generator compile ./ --local
```
If simply doing ESSL entity design work, create your .essl files in your EsslLocalSource/HelloEssl folder, generate PlantUml
diagrams (assuming you have used @umlGroup and @uml decorators - see projects in essl.combinedsource for examples) from VSCode, by right-clicking
and selecting "Generate PlantUML".  For command-line, see "ESSL.Generator verbs" below.

Once there are no errors, the service business logic project and API wrapper can be generated.  In VSCode, right click on one
of the files, select "Generate Services", select the specific services you want generated and click OK.  If using command line,
enter the command below to generated the HelloEssl project only.
```
ESSL.Generator genService ./ -l -s HelloEssl
```
This will generate the {root}/HelloEsslRepo/SB.HelloEssl/SB.HelloEssl.csproj implementation project as well as the
{root}/HelloEsslRepo/SB.helloEssl.API/SB.HelloEssl.API.csproj GraphQL wrapper project.  Of course, unless you have created one or
more .essl files in EsslLocalSource/HelloEssl, these folders will be empty since there is nothing to generate.

In the example "genService" command above, generation was only done for HelloEssl.  Unlike when compiling, where all dependencies
must also be compiled, "genService" compiles what is necessary, but only generates what has been requested.  Now would be a good
time to run the generator again and select "all", rather than "HelloEssl".  (In VSCode, make sure all boxes are checked when
generating).  You will see that the "EventStore" repo is generated, and if you drill into that folder, the EventStore
project and Protocols.Config project are generated.  Since these projects contain .essl files, the empty CQRS command, query,
and API implementations are generated and ready for adding business logic in C#.  Of course this has already been done for the
EventStore repo and is in the BitBucket EventStore repository.

At this point, if you have not already, start adding your .essl files to the HelloEsslRepo/EsslLocalSource/HelloEssl local essl source
folder and compile, generate images, and generate services as desired.  Be aware that unlike the first time a service
is generated, the "project" copy of the Essl files (e.g. HelloEsslRepo/SB.HelloEssl/ESSL/*) are not copied in from the
"local" essl files that you have been editing.  This allows you to only update the essl files in your generated project when
you are ready to.  The generator (and VSCode) contains commands for comparing local vs project files and for copying your project's
essl files from local to project (CopyTo), or from project to local (CopyFrom) when editing/initializing a project, etc.

A sample solution file <a href="https://bitbucket.org/sweetbridge/protocols.essl/src/master/HelloESSL%20Sample.sln" target="_blank">HelloESSL Sample.sln</a> exists
that can be used to bootstrap things.  Because project references are
(currently) used, certain existing projects must be included, which this sample solution does.  Copy this into
your top-level folder (in this example HelloEsslRepo) and name it the same as your folder .sln (HelloEsslRepo.sln).  You should now
be able to open the solution using Visual Studio 2019 (community or full edition) or Rider.

Once open in VS or Rider, when you compile you should get a single syntax error, saying the IServiceConfig does not contain a definition for
HelloEsslSchema.  This is because the config service does not have first class knowledge of your new project.  For now,
it is safe to comment out the offending line.  In the future, this will probably be changed to a late-bound assignment,
but that remains a TODO item.

You can now start adding business logic to the generated stubs in the Command and Model folders.  Nothing is runnable yet,
as persistence, etc. will need to be set up, which is beyond the scope of this document.
### ESSL.Generator verbs
ESSL.Generator has several verbs and quite a few options on each one.  To simply see a list of verbs, run:
```
$ ESSL.Generator --help
```
Detailed help on each verb is available by specifying the verb and --help, for example:
```
$ ESSL.Generator genTypes --help
```
Verbs are:
```
  compile         Compile ESSL, list any errors, and summarize results.
  genService      Compile and Generate selected Service frameworks in the paths specified in each .esslproj file.
  genPlantUML     Compile and Generate PlantUML files into the local project's Diagrams folder.
  compare         Compare ESSL project files in local folders to those in project folders.
  genTypes        Compile and Generate Client Types capable of being used by a GraphQL Client when calling GraphQL services.
  genArtifacts    Compile and Generate artifacts from their ESSL definition.
  help            Display more information on a specific command.
  version         Display version information.
```
* **Compile** compiles the ESSL source (based on everything discoverable from the .esslsln file), checks for syntax errors, and does
  semantic resolution, such as type resolution and other issues.  It does not generate any output other than in the terminal session, or alter any files.
* **GenService** generates both the implementation project and GraphQL API project source, ready for the stubs to be filled in.
  Upon regeneration, hand-coded content within the stubs is maintained, but any other code altered in the files will be overwritten.
  Best practice is to create your own subfolders (next to Command, ESSL, Model, etc.) and place your code in those folders.  They are
  not touched during regeneration.  See the detailed genService help for several options, such as which service(s) to generate, etc.
* **GenPlantUML** generates PlantUML (.puml) files into the local source project folders.  This is a subset of the work done by GenService
  and is provided to easily get output when modeling ESSL types in VSCode.  The files generated are based on the @umlGroup markings in the
  ESSL source files and they are placed in a "Diagrams" folder in the respective local project folder containing the ESSL source.
* **Compare** compares the local .essl and .esslproj files to those in the projects themselves.  Differences are reported to assist
  you in synchronizing them.
* **GenTypes** generates language specific types based on your ESSL definitions.  These are suitable for using in GraphQL calls.
  Two variations of CSharp types (including Relay support) as well as Kotlin currently exist.  Generating for additional languages
  is relatively easy to add primarily by creating new ESSL.Template files.
* **GenArtifacts** generates other artifacts, such as PlantUml images (as png or svg), and rudimentary Razor web pages to bootstrap
  WebSite development.
### ESSL.Generator Common Options
In addition to options specific to each of the above verbs, there are a few common options, including:
* **-s \<SERVICES>**, which allows the verbs to be applied to one or more services in the list.  The special service name of "all" causes
  the verb to apply to all services.  Treatment varies slightly by verb.
* **--local** and **--project**, or -l and -p, this tells ESSL.Generator which set of ESSL source to compile.
* **-t \<TEMPLATES>**, allows specification of an alternate set of templates, such as when experimenting with changing how something is generated.
* **--verbose**, reports additional information.  This is useful for troubleshooting.
* **-c \<PROFILE>**, allows the output color scheme to be varied, or turned off.  Different Terminal implementations on different platforms
  treat colors differently, resulting in text that is sometimes difficult to read.  Choose from Light, Dark, Windows, Default, and None to find
  what works best if this is an issue.
* **-o**, allows the specification of a generated output folder.
* **-v**, for many verbs outputs addition diagnostic information that can help in understanding.

