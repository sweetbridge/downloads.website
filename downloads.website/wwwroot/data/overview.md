﻿# ESSL Overview
## ESSL - EventSource Specification Language

ESSL is a high-level language that is used to specify data structures and APIs, which is then used to generate
source code including language specific data structures, GraphQL Type and API definitions, PlantUML images, etc.  The generated
code and stubs enforce an EventSource pattern, enabling an immutable data store, querying history, etc.

The ESSL ecosystem includes various elements, which work together.
1.  At the lowest level is the ESSL generator, which includes the language definition, compiler, and generator.
    *  It is compiled and needs to be installed based on the target operating system, including Windows, OSX, and Linux.
    *  ESSL.Generator is a command-line program enabling compilation, code/artifact generation, and other utilities.
    *  The Generator operates on ESSL solutions, which are composed of ESSL projects, which are composed of related ESSL source files.
    *  More information on the ESSL language, structure, and running the Generator can be found [here](essl/protocols-essl).
2.  To simplify things, there is a VSCode extension for ESSL.  It brings ESSL to life with syntax highlighting, code completion and navigation.
    Menu options are provided for navigation, compilation, generating code or artifacts, etc.  This is described in more detail [here](essl/vscode-extension).
3.  A <a href="https://bitbucket.org/sweetbridge/essl.combinedsource/src/master/" target="_blank">repository</a>
    containing the ESSL combined source for the Sweetbridge protocols is also available.
    While it is more than what is typically needed when starting an ESSL project, parts of it (starting with the EventStore project) will be desirable to include in your solution
    so that you can reference services and types that have already been created.
    
Read the [documentation](essl/protocols-essl) presented on the following pages, download the appropriate [packages](essl/downloads), follow the [installation instructions](essl/generator) to begin your ESSL journey.