﻿# ESSL Combined Source

While the ESSL Source for each project lives in that project (and therefore in the repository containing that project),
in order to compile a project's ESSL, the source for all dependent project must be compiled.  This is because ESSL does not
have the concept of a package manager, such as npm or NuGet.

To make things easier, the ESSL.Generator gives you a choice of compiling ESSL from the distributed projects (--project) or
from a combined local folder (--local).  Of course this means your local ESSL source could get out of sync with
the distributed project source, so ESSL.Generator provides a "compare" command that reports any differences between the
local and project sources, as well as the ability to copy "to project" or "from project", which copies the ESSL source
files from local "to" the project location, or "from" the project location to the local location.  This mechanism can
be used to help synchronize the two locations.  See [ESSL Documentation](essl/protocols-essl) for more detail.

A public repository has been created called 
<a href="https://bitbucket.org/sweetbridge/essl.combinedsource/src/master" target="_blank">ESSL.CombinedSource</a>
which holds the combined source from across all ESSL-based projects.  This makes it easy to navigate around, compile, and generate
artifacts directly from VSCode.

## Installation

Clone the <a href="https://bitbucket.org/sweetbridge/essl.combinedsource/src/master" target="_blank">ESSL.CombinedSource Repo</a> 
and open the folder with VSCode.  For
best results use both the ESSL and PlantUML extensions for VSCode. 