# ESSL

## Installation of the ESSL Generator

Before you run any essl files, you need to install the essl generator. To do this, follow these instructions:

First, download the latest essl.generator zip from [Downloads](essl/downloads) for your respective OS.
Then, follow the instructions below based on your operating system.

##### Mac:
1. Create the contents of the directory "osx-x64" inside the downloads zip file in /usr/local/ called "essl"
2. Copy the contents of the installation zip into this essl directory
3. Add this directory containing the compiled essl.generator to PATH variable.
    - Adding to path on macOS:
        - Open terminal, "cd ~"
        - Check if the ".zshrc" file exists by typing "ls -la" (or .zprofile if you prefer)
        - If it does not, type "touch .zshrc" (or "touch .zprofile")
        - Open the file manually via finder, and change the contents of this file to be "export PATH=$PATH:/usr/local/essl" or add ":/usr/local/essl" to the end if the file already exists
        - The directory containing the essl.generator is now in PATH and be accessed simply by typing "essl.generator" in a terminal
  
##### Windows: 
1. Copy the contents of the directory "win-x64..." directory inside the installation zip into a directory in ProgramFiles called "Essl" or similar.
2. Add this directory containing the compiled essl.generator to PATH variable
    - Adding to the path on windows:
      - Search for "edit the system environment variables"
      - Click "Environment variables" in the bottom right of the window that opened
      - Find "Path" in System variables, click edit.
      - Add the absolute path to the directory, click ok.
      - The directory containing the essl.generator is now in PATH and be accessed simply by typing "essl.generator" in a terminal