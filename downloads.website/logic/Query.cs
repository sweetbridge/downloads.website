﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace downloads.website.Logic
{
    public class Query
    {
        public static Dictionary<string, string> content = new();

        public static async Task<string> query(string url, bool local)
        {
            if (url != null)
            {
                if (get(url) != null)
                {
                    return content[url];
                }

                // using var client = new HttpClient();
                HttpClient client;
                if (local)
                {
                    client = new HttpClient { BaseAddress = Program.BaseAddress };
                }
                else
                {
                    client = new HttpClient();
                }
                using (client)
                {
                    var response = await client.GetStringAsync(url);
                    if (content != null)
                    {
                        content.Add(url, Markdig.Markdown.ToHtml(response));
                    }
                }
            }

            return get(url);
        }

        public static string get(string key)
        {
            if (key == null)
            {
                return null;
            }

            try
            {
                var value = content[key];
                return value;
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }
    }
}