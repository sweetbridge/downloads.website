:: ssh into the server
ssh sb-emea-ec2-essldownloads

:: Stop and remove the old container, prune old images, clone the repo
docker container stop downloads.website.container
docker container rm downloads.website.container

docker image prune -f

cd ~

rm -r -f downloads.website

git clone https://charliebaird@bitbucket.org/sweetbridge/downloads.website.git

cd downloads.website/downloads.website

:: Build the docker image and run it. The Dockerfile is configured to 'dotnet build'
:: and to 'dotnet publish' so this is not necessary here.
docker build -t downloads.website .

:: Run the newly created docker image, specifying the volumes that contain the certificates for letsencrypt https certification
:: These volumes are references in nginx.conf
docker run -d --name downloads.website.container \
    -p 443:443 -p 80:80 \
    -v /etc/letsencrypt:/etc/letsencrypt \
    -v /etc/letsencrypt/live/downloads.sweetbridge.com:/etc/letsencrypt/live/downloads.sweetbridge.com \
    downloads.website

cd ~