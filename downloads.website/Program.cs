using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace downloads.website
{
    public class Program
    {
        public static Uri BaseAddress;
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
            var client = new HttpClient { BaseAddress = BaseAddress };
            builder.Services.AddScoped(sp => client);

            await builder.Build().RunAsync();
        }
    }
}
