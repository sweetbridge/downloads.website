# Downloads.Website

## Instructions for deployment of blazor with certificates:
- Create the new instance on Amazon AWS
- Get the ssh keyfile to ssh into the new machine
- Once ssh'd, clean up the new machine's files to ensure space for the project (only necessary if cloned from another project.)
    - If not cloned, installing docker is necessary.
- Even if you can ssh, try pinging the new server. There may be an issue with the security of the server, change this on AWS config page. It should show a warning if there is an issue. This was an issue that we can with this page.
- Clone the new blazor repo, and follow the general outline of the commands below inside instructions for redeployment. Also look at the ```Dockerfile``` and ```nginx.conf``` for ideas.
- Use letsencrypt to get certificates inside the /etc/letsencrypt/live/ folder.
- In the docker run command, make sure you include both of the following volumes, changing "downloads.sweetbridge.com" to the new name. If you don't, nginx and docker will throw an error saying the https certificates were not found.
```
-v /etc/letsencrypt:/etc/letsencrypt
```
and
```
-v /etc/letsencrypt/live/downloads.sweetbridge.com:/etc/letsencrypt/live/downloads.sweetbridge.com
```

## Instructions for redeployment:
- These commands are also in ```DockerCreateAndRun.cmd```. Note you can't simply double click the script, the ssh will cancel all commands below it in the script.
- ssh into the remote machine
```
ssh sb-emea-ec2-essldownloads
```
Remove the old container, build a new one with the new image and run it.
```
docker container stop downloads.website.container
docker container rm downloads.website.container

docker image prune -f

cd ~

rm -r -f downloads.website

git clone https://charliebaird@bitbucket.org/sweetbridge/downloads.website.git

cd downloads.website/downloads.website

docker build -t downloads.website .

docker run --name downloads.website.container \
    -p 443:443 -p 80:80 -d \
    -v /etc/letsencrypt:/etc/letsencrypt \
    -v /etc/letsencrypt/live/downloads.sweetbridge.com:/etc/letsencrypt/live/downloads.sweetbridge.com \
    downloads.website
```

## Updating download links
- The page is coded to query the [downloads.json](https://api.bitbucket.org/2.0/repositories/sweetbridge/downloads.website/src/master/downloads.website/wwwroot/downloads.json) file inside wwwroot
- Update this file and make sure it is pushed to the latest commit on ```master```
- Redeployment is not necessary if this is the only change.

## Obtaining download links for Google drive files
- Go to drive.google.com and right click on the desired file.
- Click "Get link"
  - Make sure "Anyone on the internet with this link can view" is set.
  - Copy the link
- Go to https://sites.google.com/site/gdocs2direct/home
  - Paste the link in "Enter your sharing URL"
  - Click "Create Direct Link", and copy the "Output link"
- Edit wwwroot/downloads.json and paste the new link in the appropriate file.
- Commit the change.
  - As long as only the links are updated (files are not added, removed, or renamed), redeployment is not necessary as the downloads.json file is read from
  the repository and will automatically pick up the change.